const Redis = require("ioredis")
import db from './NedbUtils'

let redisConnectList = {}

class RedisUtils {

    constructor() {
    }

    static getRedisConnect(clientId, dbNum, callback) {
        if (redisConnectList[clientId] === undefined || redisConnectList[clientId] === null) {
            db.findOne({_id: clientId}, function (err, doc) {
                if (doc != null) {
                    if (doc.port == null || doc.url === '') {
                        return callback(null)
                    }
                    if (doc.port === '') {
                        doc.port = "6379"
                    }
                    if (doc.db === '') {
                        doc.db = "0"
                    }
                    let client = new Redis({
                        port: doc.port,
                        host: doc.url,
                        password: doc.password,
                        db: dbNum,
                        lazyConnect: true,
                        connectTimeout: 3000
                    })
                    client.connect(function (err, result) {
                        if (err == null) {
                            redisConnectList[clientId] = client
                            return callback(client)
                        } else {
                            client.disconnect()
                            return callback(null)
                        }
                    })
                }
            });
        } else {
            let client = redisConnectList[clientId]
            client.select(dbNum, function (err, result) {
                console.log(client)
                return callback(client)
            })
        }
    }

    static testConnect(config, callback) {
        if (config.port === null || config.url === '') {
            return callback(false)
        }
        if (config.port === '') {
            config.port = "6379"
        }
        if (config.db === '') {
            config.db = "0"
        }
        let client = new Redis({
            port: config.port,
            host: config.url,
            password: config.password,
            db: 0,
            lazyConnect: true,
            connectTimeout: 3000
        })
        client.connect(function (err, result) {
            if (err == null) {
                client.disconnect()
                return callback(true)
            } else {
                client.disconnect()
                return callback(false)
            }
        })
    }
}

export default RedisUtils

