import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

// 解决重复点击导航路由报错
const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err);
}

export default new Router({
    routes: [
        {
            path: '/',
            name: 'landing-page',
            component: resolve => require(['@/view/Index'], resolve)
        },
        {
            path: '/index',
            redirect: 'index',
            component: resolve => require(['@/view/Index'], resolve),
            children: [
                {
                    path: 'redis',
                    name: 'redis',
                    component: resolve => require(['@/view/redis/Redis'], resolve)
                }
            ]
        },
        {
            path: '*',
            redirect: '/'
        }
    ]
})
