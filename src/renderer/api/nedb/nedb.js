import db from '../../utils/NedbUtils'

export const insertData = (doc, callback) => {
    return db.insert(doc, function (err, newDoc) {
        callback(newDoc)
    });
}

export const deleteData = (id, callback) => {
    return db.remove({_id: id}, {}, function (err, numRemoved) {
        callback(numRemoved)
    });
}

export const updateData = (id, doc, callback) => {
    return db.update({_id: 'id'}, doc, {}, function (err, numReplaced) {
        callback(numReplaced)
    });
}

export const selectData = (id, callback) => {
    return db.findOne({_id: id}, function (err, doc) {
        callback(err, doc)
    });
}

export const listData = (doc, callback) => {
    return db.find(doc, function (err, docs) {
        callback(docs)
    });
}
