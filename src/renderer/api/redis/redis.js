import RedisUtils from '../../utils/RedisUtils'

export const getConnect = (clientId, dbId, callback) => {
    RedisUtils.getRedisConnect(clientId, dbId, (client) => {
        return callback(client)
    })
}

export const testConnect = (config, callback) => {
    return RedisUtils.testConnect(config,(err) => {
        return callback(err)
    })
}


export const getDbs = (clientId, callback) => {
    RedisUtils.getRedisConnect(clientId, 0, (client) => {
        if (client == null) {
            return callback(null, null)
        }
        return client.config("get", "databases", function (err, databases) {
            client.info("Keyspace", function (err, keyspace) {
                callback(databases, keyspace)
            })
        })
    })
}

export const getScanKeys = (clientId, db, match, size, callback) => {
    let keyList = []
    RedisUtils.getRedisConnect(clientId, db, (client) => {
        let stream = client.scanStream({
            match: match,
            count: size
        });
        stream.on("end", function () {
        });
        return stream.on("data", function (resultKeys) {
            for (const key of resultKeys) {
                client.type(key, function (err, type) {
                    if ("string" == type) {
                        client.strlen(key, function (err, size) {
                            keyList.push({
                                name: key,
                                type: type,
                                size: size
                            })
                        })
                    }
                })
            }
            callback(keyList)
        });
    })
}

export const getKeyType = (clientId, db, key, callback) => {
    RedisUtils.getRedisConnect(clientId, db, (client) => {
        return client.type(key, function (err, result) {
            callback(result)
        })
    })
}

export const getData = (clientId, db, key, callback) => {
    RedisUtils.getRedisConnect(clientId, db, (client) => {
        return client.get(key, function (err, result) {
            callback(result)
        })
    })
}


export const setKeyValue = (clientId, db, keyForm, callback) => {
    if (keyForm == null) {
        return
    }
    RedisUtils.getRedisConnect(clientId, db, (client) => {
        return client.set(keyForm.name, keyForm.value, function (err, result) {
            callback(result)
        })
    })
}

export const deleteKey = (clientId, db, key, callback) => {
    if (key == null) {
        return
    }
    RedisUtils.getRedisConnect(clientId, db, (client) => {
        return client.del(key, function (err, result) {
            callback(result)
        })
    })
}






export const setData = (client, doc, value) => {
    return client.set(key, value)
}


export const changeDb = (client, db, callback) => {
    return client.select(db, function (err, result) {
        callback(err, result, client)
    })
}

export const getKeys = (clientId, db, callback) => {
    RedisUtils.getRedisConnect(clientId, db, (client) => {
        return client.keys("*", function (err, result) {
            callback(result)
        })
    })
}

export const getKeySize = (client, key, type, callback) => {
    if ("string" == type) {
        console.log(type)
    }
    return client.strlen(key, function (err, result) {
        callback(err, result, client)
    })
}