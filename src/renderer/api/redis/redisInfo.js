import RedisUtils from '../../utils/RedisUtils'
import nedb from '../../utils/NedbUtils'

export const getInfo = (clientId, db, callback) => {
    RedisUtils.getRedisConnect(clientId, db, (client) => {
        return client.info(function (err, result) {
            let split = result.split("\n");
            let data = []
            for (const splitElement of split) {
                if (splitElement.search("#") == -1) {
                    let strings = splitElement.split(":");
                    data.push({
                        name: strings[0],
                        value: strings[1]
                    })
                }
            }
            callback(data)
        })
    })
}

export const redisCommandMonitor = (clientId, db, callback) => {
    RedisUtils.getRedisConnect(clientId, db, (client) => {
        return client.monitor(function (err, monitor) {
            monitor.on("monitor", function (time, args, source, database) {
                let doc = {
                    catalog: "redis",
                    host: client.options.host,
                    datetime: time.split(".")[0],
                    args: args,
                    source: source,
                    database: database
                }
                nedb.insert(doc, function (err, newDoc) {
                    callback(newDoc)
                });
            });
        });
    })
}
//1582553324
export const refreshRedisCommandMonitor = (clientId, time, callback) => {
    return nedb.find({catalog: "redis", _id: clientId}, function (err, docs) {
        nedb.find({catalog: "redis", host: docs[0].url, datetime: time}, function (err, docs) {
            callback(docs)
        });
    });

}

