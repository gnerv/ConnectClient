import db from '../../utils/NedbUtils'

export const saveRedisData = (doc, callback) => {
    if (doc._id == null || doc._id == '') {
        return db.insert(doc, function (err, newDoc) {
            callback(newDoc)
        });
    } else {
        return db.update({_id: doc._id}, doc, {}, function (err, numReplaced) {
            callback(numReplaced)
        });
    }
}
export const insertRedisData = (doc, callback) => {
    return db.insert(doc, function (err, newDoc) {
        callback(newDoc)
    });
}

export const deleteRedisData = (id, callback) => {
    return db.remove({_id: id}, {}, function (err, numRemoved) {
        callback(numRemoved)
    });
}

export const updateRedisData = (id, doc, callback) => {
    return db.update({_id: 'id'}, doc, {}, function (err, numReplaced) {
        callback(numReplaced)
    });
}

export const selectRedisData = (id, callback) => {
    return db.findOne({_id: id}, function (err, doc) {
        callback(err, doc)
    });
}

export const listRedisData = (doc, callback) => {
    db.find({}).sort({}).exec(function (err, docs) {
        // console.log(docs)
    });
    doc.catalog = 'redis'
    return db.find(doc).sort({type: -1, name: 1}).exec(function (err, docs) {
        callback(docs)
    });
}
